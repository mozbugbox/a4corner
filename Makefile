TABLE_DIR = $(shell /usr/bin/pkg-config --variable tabledir ibus-table)
ICON_DIR = $(shell /usr/bin/pkg-config --variable icondir ibus-table)
IBUS_EXEC_DIR = $(shell /usr/bin/pkg-config --variable exec_prefix ibus-table)

TABLE_NAME = a4corner
ICONS = ${TABLE_NAME}.svg
TABLE = ${TABLE_NAME}.db
TABLE_TXT = ${TABLE_NAME}.table.txt
CREATOR = ${IBUS_EXEC_DIR}/bin/ibus-table-createdb

SOURCES = a4corner.table.template \
		  vimim.cjk.txt \
		  parse-4corner-data.py \
		  4corner-27585.txt \
		  sogou-freq-all.txt
DOCS = README

INSTALL = /usr/bin/install -c
INSTALLDATA = /usr/bin/install -c -m 644

all: $(TABLE)

$(TABLE_TXT): $(SOURCES)
	./parse-4corner-data.py

%.db: %.table.txt
	$(CREATOR) -n $@ -s $<

install:
	$(INSTALLDATA) $(TABLE) $(TABLE_DIR)
	$(INSTALLDATA) $(ICONS) $(ICON_DIR)

dist: $(SOURCES)
	VERSION=`egrep "^SERIAL_NUMBER\s*=" $(TABLE_NAME).table.template|sed "s/.*=\s*//"`; \
			DIST_DIR=$(TABLE_NAME)-$${VERSION}; \
			/bin/mkdir $${DIST_DIR}; \
			/bin/cp $(ICONS) $(SOURCES) $(DOCS) Makefile $${DIST_DIR}; \
			/bin/tar -jvcf $${DIST_DIR}.tar.bz2 $${DIST_DIR}; \
			/bin/rm -rf $${DIST_DIR}

clean:
	rm -f $(TABLE_TXT)
	rm -f $(TABLE)

.PHONY: all install dist clean
