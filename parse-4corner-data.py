#!/usr/bin/python
# vim:fileencoding=utf-8:sw=4:et

from __future__ import print_function, unicode_literals, absolute_import
import sys
import os, io
import logging as log

CODELENGTH = 4
VIMFILE = "vimim.cjk.txt"
INFILE = VIMFILE
INFILE = "4corner-27585.txt"
OUTFILE = "a4corner.table.txt"
TEMPLATE = "a4corner.table.template"
FREQ_FILE = "sogou-freq-all.txt"
DO_ALPHA = True

ALPHA_FROM_NUM = {
        0: "l",
        1: "h",
        2: "s",
        3: "d",
        4: "v",
        5: "c",
        6: "k",
        7: "j",
        8: "b",
        9: "x",
        }

def map2alpha(code4):
    """Map 4corner to alpha symbols."""
    item = []
    for digit in code4:
        if digit == ".":
            item.append(digit)
        else:
            num = int(digit)
            alpha = ALPHA_FROM_NUM[num]
            item.append(alpha)
    result = "".join(item)
    return result

def load_frequency():
    freq_file = FREQ_FILE
    freq_map = {}
    with io.open(freq_file, encoding="UTF-8") as fd:
        for line in fd:
            parts = line.strip().split(None, 1)[:2]
            uchar = parts[0]
            freq = parts[1]
            freq_map[uchar] = freq
    return freq_map

def main():
    log_level = log.INFO
    log.basicConfig(format="%(levelname)s>> %(message)s", level=log_level)
    result = []
    with io.open(INFILE, encoding="UTF8") as fd:
        for line in fd:
            chars, code4 = line.split(None, 2)[:2]
            if DO_ALPHA:
                code4 = map2alpha(code4)
            for char in chars:
                result.append((code4, char))
    result.sort(key=lambda x:x[1])
    freq_map = load_frequency()
    txt = []
    for code4, char in result:
        freq = freq_map.get(char, 0)
        txt.append("{}\t{}\t{}".format(code4[:CODELENGTH], char, freq))
    txt = "\n".join(txt)+"\n"

    log.info("Writting to {}...".format(OUTFILE))
    fdtemp = io.open(TEMPLATE)
    with io.open(OUTFILE, "w", encoding="UTF8") as fdout:
        for line in fdtemp:
            fdout.write(line)
            if line.startswith("BEGIN_TABLE"):
                fdout.write(txt)

if __name__ == '__main__':
    main()

